# Take home assignment works with both local files and github gists.

### notes:

- requires spacy and requests.  install however you usually install (could be via docker/conda/virtualenv) or for generic install instruction using python3:

```
pip3 install -U spacy requests
python3 -m spacy download en
```

## usage:
```
GrahamAnnettAllenAI $ python3 main.py -h
usage: main.py [-h] -d DOCUMENTS [DOCUMENTS ...] -w WORDS [WORDS ...]

Takes as input a set of documents/url, and list of words and returns the
document with the highest TF score for each word

optional arguments:
  -h, --help            show this help message and exit
  -d DOCUMENTS [DOCUMENTS ...], --documents DOCUMENTS [DOCUMENTS ...]
                        gist url of documents or folder of documents
  -w WORDS [WORDS ...], --words WORDS [WORDS ...]
                        list of words to run term frequency on
```

Example:

`python3 main.py -d https://gist.github.com/schmmd/5900e57fe1f8359d5884bbe3baa55429 -w queequeg whale sea`

or if files in directory test_docs

`python3 main.py -d test_docs -w queequeg whale sea`

# Output
```
Word: queequeg, tf high score is 0.006555423122765197, with document mobydick-chapter4.txt
Word: whale, tf high score is 0.001328609388839681, with document mobydick-chapter1.txt
Word: sea, tf high score is 0.005757307351638618, with document mobydick-chapter1.txt
```