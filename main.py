import os
import argparse
from collections import Counter

import requests
import spacy

def get_args():
    parser = argparse.ArgumentParser(description='''Takes as input a set of
        documents/url, and list of words and returns the document with the
        highest TF score for each word''', add_help=True)

    # set of documents
    parser.add_argument('-d', '--documents', action="store", dest="documents",
                        nargs='+', required=True,
                        help='gist url of documents or folder of documents')

    # list of words
    parser.add_argument('-w', '--words', action="store", dest="words",
                        nargs='+', required=True,
                        help='list of words to run term frequency on')

    return parser.parse_args()


class DatasetReader:
    """
    A DatasetReader would read documents from a folder or url

    """

    def __init__(self):
        self.documents = []

    def get_documents_from_github_gist(self, url):
        gist_id = url.split('/')[-1]
        api_url = 'https://api.github.com/gists/' + gist_id
        resp = requests.get(api_url).json()
        for file in resp['files'].keys():
            self.documents.append(TextDocument(
                resp['files'][file]['content'],
                resp['files'][file]['filename']))

    def get_documents_from_folder(self, filepath):
        files = os.listdir(filepath)
        for file in files:
            with open(os.path.join(filepath, file), 'r') as txt:
                self.documents.append(TextDocument(txt.read(), file))

    def read(self, locations):
        for location in locations:
            if os.path.isdir(location):
                self.get_documents_from_folder(location)
            elif 'gist.github.com' in location:
                self.get_documents_from_github_gist(location)
            else:
                raise NotImplementedError


class TextDocument:
    """
    TextDocument is a single instance of text that has been parsed by spacy or
    another parser
    """

    def __init__(self, text, name):
        self.doc = nlp_spacy(text.lower())
        self.doc_name = name
        self.words = [
            token.text for token in self.doc if token.is_punct is False]
        self.word_count = Counter(self.words)

    def calculate_score_for_word(self, word):
        # get() returns None
        numerator = self.word_count.get(word)
        if numerator is None:
            return 0
        return numerator / len(self.words)


if __name__ == '__main__':
    parser = get_args()

    # load after get_args since slowest part
    nlp_spacy = spacy.load('en')

    dataset = DatasetReader()
    dataset.read(parser.documents)
    for word in parser.words:
        high_score = 0
        doc_name = ''
        for doc in dataset.documents:
            cur_score = doc.calculate_score_for_word(word)
            if cur_score > high_score:
                high_score = cur_score
                doc_name = doc.doc_name
        print('Word: {}, tf high score is {}, with document {}'.format(
            word, high_score, doc_name))
